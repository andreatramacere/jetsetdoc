MinutiMinimizer
===============

.. currentmodule:: jetset.minimizer

.. autoclass:: MinutiMinimizer
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~MinutiMinimizer.chisq_func
      ~MinutiMinimizer.get_chisq

   .. rubric:: Methods Documentation

   .. automethod:: chisq_func
   .. automethod:: get_chisq
