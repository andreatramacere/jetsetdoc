SEDShape
========

.. currentmodule:: jetset.sed_shaper

.. autoclass:: SEDShape
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~SEDShape.IC_fit
      ~SEDShape.add_BBB_template
      ~SEDShape.add_disk
      ~SEDShape.add_host_template
      ~SEDShape.check_adapt_range_size
      ~SEDShape.do_sync_fit
      ~SEDShape.eval_indices
      ~SEDShape.find_class
      ~SEDShape.get_nu_max
      ~SEDShape.plot_indices
      ~SEDShape.save_sync_fit_report
      ~SEDShape.set_S_LE_slope
      ~SEDShape.show_values
      ~SEDShape.sync_fit

   .. rubric:: Methods Documentation

   .. automethod:: IC_fit
   .. automethod:: add_BBB_template
   .. automethod:: add_disk
   .. automethod:: add_host_template
   .. automethod:: check_adapt_range_size
   .. automethod:: do_sync_fit
   .. automethod:: eval_indices
   .. automethod:: find_class
   .. automethod:: get_nu_max
   .. automethod:: plot_indices
   .. automethod:: save_sync_fit_report
   .. automethod:: set_S_LE_slope
   .. automethod:: show_values
   .. automethod:: sync_fit
