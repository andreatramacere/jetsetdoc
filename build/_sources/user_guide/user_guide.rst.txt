
JetSeT User Guide
=================

 

Content
-------------------------

.. toctree::
   :maxdepth: 1

   jet model <jet_model/Jet_example.rst>
   model fitting  <model_fit/fit_example.rst>

 

   
